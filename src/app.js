import Koa from 'koa'
import middleware from './middleware';
import hash from './lib/hash';
import router from './router';

const app = new Koa();

Object.entries(middleware).map(([$, m]) => app.use(m));
app.use(router.routes());
app.use(router.allowedMethods());

app.listen({port: 3000});
console.log('Koa server listening on 0.0.0.0:3000');
