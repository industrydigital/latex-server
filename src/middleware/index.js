import id from './id';
import errors from './errors';

export default {
  id,
  errors,
};
