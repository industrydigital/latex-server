import uuid from 'uuid';

export default async ({request}, next) => {
  request.headers['X-Request-ID'] = uuid.v4();
  await next();
};

