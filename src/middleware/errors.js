export default async (ctx, next) => {
  let {request, response} = ctx;
  try {
    await next();
  }
  catch (e) {
    response.status = 500;
    response.body = e.message;
    ctx.app.emit('error', e, ctx);
  }
};

