import uuid from 'uuid';
import fs from 'fs';
import subprocess from 'child_process';
import pathlib from 'path';
import {promisify} from 'util';

const {env} = process;
const exec = promisify(subprocess.exec);
const mkdir = promisify(fs.mkdir);
const writefile = promisify(fs.writeFile);
const readfile = promisify(fs.readFile);
const stat = promisify(fs.stat);

////////////////////////////////////////////////////////////////////////////////

const assert_buffer = (input) => {
  let is_buffer = input instanceof Buffer;
  let is_string = typeof input === 'string';
  if (!is_string && !is_buffer) {
    throw new Error('expected String or Buffer input argument');
  }
};

const create_workdir = async (dirname=uuid.v4(), tmp=env.LATEX_SERVER_TEMP_DIR) => {
  let dirpath = `${tmp}/${dirname}`;
  await mkdir(dirpath, {recursive: true, mode: 0o777});
  return dirpath;
};

const create_cli_command = (binary, infile, options) => {
  let serialize = ([k, v]) => v ? `-${k}=${String(v)}` : `-${k}`;
  let tokens = Object.entries(options).map(serialize);
  tokens.unshift(binary);
  tokens.push(infile);
  return tokens.join(' ');
};

const ALLOWED_XETEX_OPTIONS = [
  'etex',
  'file-line-error',
  'no-file-line-error',
  'mltex',
  'no-pdf',
  'parse-first-line',
  'no-parse-first-line',
  'papersize',
  '8bit',
];

const prepare_cli_options = (options, overrides) => {
  let defaults = {
    'papersize': 'A4',
    'halt-on-error': null,
  };

  let allowed = ([k, v]) => ALLOWED_XETEX_OPTIONS.indexOf(k) > -1;
  let safe = Object.entries(options).filter(allowed).reduce((obj, [k, v]) => {
    obj[k] = v;
    return obj;
  }, {});

  return {...defaults, ...safe, ...overrides};
};

////////////////////////////////////////////////////////////////////////////////

export default async (input, options={}) => {
  assert_buffer(input);

  let workdir = await create_workdir();
  let infile = `${workdir}/document.tex`;
  let outfile = `${workdir}/document.pdf`;

  // write input buffer to a temporary file in temporary workdir
  await writefile(`${infile}`, input);

  let opts = prepare_cli_options(options, {
    'interaction': 'batchmode',    
    'output-directory': workdir,
  });

  let cmd = create_cli_command('xelatex', pathlib.basename(infile), opts);

  try {
    let {stdout, stderr} = await exec(cmd, {cwd: workdir});
    let [stats, buffer] = await Promise.all([stat(outfile), readfile(outfile)]);
    exec(`rm -rf ${workdir}`); 
    return {stats, buffer, stdout, stderr}
  }
  catch (e) {
    let log = await readfile(`${workdir}/document.log`, 'utf8');
    let err = new Error(log);
    exec(`rm -rf ${workdir}`); 
    throw err;
  }

};
