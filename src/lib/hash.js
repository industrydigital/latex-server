import crypto from 'crypto';

export const digest = (buffer, algo='sha256', options={}) => {
  let hash = crypto.createHash('sha256', options);
  hash.update(buffer);
  return hash.digest('hex');
};

export default {
  digest,
};
