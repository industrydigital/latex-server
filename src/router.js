import Router from 'koa-router';
import handlers from './handlers';

const router = new Router();

router.post('/xelatex', handlers.xelatex);

export default router;
