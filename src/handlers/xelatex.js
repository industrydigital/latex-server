import raw from 'raw-body';
import xelatex from '../lib/latex/xelatex';

export default async (ctx, next) => {
  let {req, request, response} = ctx;
  let input = await raw(req);
  let output = await xelatex(input);

  response.set('Content-Type', 'application/pdf');
  response.body = output.buffer;

  await next();
};

