FROM industrydigital/buildpack-node:11.10.0 as dev

ENV LATEX_SERVER_TEMP_DIR "/tmp/latex-server"
ENV TEXLIVE_HOME "/usr/share/texlive"
ENV TEXLIVE_PLATFORM "x86_64-linux"
ENV PATH "$PATH:$TEXLIVE_HOME/bin/$TEXLIVE_PLATFORM"

RUN set -xeu \
    && mkdir -p $LATEX_SERVER_TEMP_DIR \
    && chown -R $RUNTIME_USER:$RUNTIME_GROUP $LATEX_SERVER_TEMP_DIR \
    && apt-get update \
    && apt-get install -y libfontconfig wget

RUN set -xeu \
    && WORKDIR=/tmp/workdir \
    && mkdir -p $WORKDIR \
    && cd $WORKDIR \
    && curl -sL 'https://github.com/yihui/tinytex/raw/master/tools/texlive.profile' > texlive.profile \
    && curl -sL 'http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz' > texlive.tgz \
    && tar -xzf texlive.tgz \
    && mkdir texlive \
    && cd texlive \
    && TEXLIVE_INSTALL_ENV_NOCHECK=true \
        TEXLIVE_INSTALL_NO_WELCOME=true \
        $WORKDIR/install-tl-*/install-tl -profile=$WORKDIR/texlive.profile \
    && cd $WORKDIR \
    && chown -R root:root texlive \
    && rm -rf $TEXLIVE_HOME \
    && mv texlive $TEXLIVE_HOME \
    && cd /tmp \
    && rm -rf $WORKDIR \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && tlmgr update --list || tlmgr option repository ctan \
    && tlmgr install \
        collection-fontsrecommended \
        fontspec \
        geometry \
        latex-bin \
        lipsum \
        luatex \
        xetex 

VOLUME ["$LATEX_SERVER_TEMP_DIR"]

###############################################################################

FROM dev as dist

WORKDIR /project
COPY . /project

RUN npm install --production \
    && chown -R runtime:runtime /project

USER runtime

CMD ["node", "src/index.js"]

###############################################################################
